import cv2

img = cv2.imread('../slike/baboon.bmp')

bordersize = 10
border = cv2.copyMakeBorder(
    img,
    top=bordersize,
    bottom=bordersize,
    left=bordersize,
    right=bordersize,
    borderType=cv2.BORDER_CONSTANT,
    value=[0, 0, 0]
)

cv2.imwrite("border.jpg", border)
