## Dodavanje šuma

***Kod***

```python
import numpy as np
import cv2
import matplotlib.pyplot as plt

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def uniform_noise(img, low, high):
  out = img.astype(np.float32)
  noise = np.random.uniform(low,high, img.shape)
  out = out + noise
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def savehist(img, filename):
  hist,bins = np.histogram(img.flatten(), bins=256, range=(0,255))
  plt.vlines(np.arange(len(hist)), 0, hist)
  plt.title("Histogram")
  plt.savefig(filename)
  
img = cv2.imread('../../slike/boats.bmp', 0)

sigmas = [1, 5, 10, 20, 40, 60]
uniform = [20, 40, 60]
salt_n_pepper = [5, 10, 15, 20]

for sigma in sigmas:
    img_new = gaussian_noise(img, 0, sigma)
    cv2.imwrite('boats_gaussian_sigma_' + str(sigma) + '.jpg', img_new)
    savehist(img_new, 'hist_gaussian_sigma_' + str(sigma) + '.jpg')
    
for limit in uniform:
    img_new = uniform_noise(img, -1 * limit, limit)
    cv2.imwrite('boats_uniform_' + str(-1 * limit) + '_' + str(limit) + '.jpg', img_new)
    savehist(img_new, 'hist_uniform_' + str(-1 * limit) + '_' + str(limit) + '.jpg')
    
for percent in salt_n_pepper:
    img_new = salt_n_pepper_noise(img, percent)
    cv2.imwrite('boats_salt_and_pepper_' + str(percent) + '.jpg', img_new)
    savehist(img_new, 'hist_salt_and_pepper_' + str(percent) + '.jpg')
```



***Rezultati (slika + histogram)***

Gaussov šum (redom sigma = [1, 5, 10, 20, 40, 60])

![Sigma1](noise/boats_gaussian_sigma_1.jpg)
![Sigma1](noise/hist_gaussian_sigma_1.jpg)

![Sigma5](noise/boats_gaussian_sigma_5.jpg)
![Sigma5](noise/hist_gaussian_sigma_5.jpg)

![Sigma10](noise/boats_gaussian_sigma_10.jpg)
![Sigma10](noise/hist_gaussian_sigma_10.jpg)

![Sigma20](noise/boats_gaussian_sigma_20.jpg)
![Sigma20](noise/hist_gaussian_sigma_20.jpg)

![Sigma40](noise/boats_gaussian_sigma_40.jpg)
![Sigma40](noise/hist_gaussian_sigma_40.jpg)

![Sigma60](noise/boats_gaussian_sigma_60.jpg)
![Sigma60](noise/hist_gaussian_sigma_60.jpg)



Uniformni šum (redom granice [(-20, 20), (-40, 40), (-60, 60)])

![Uniform20](noise/boats_uniform_-20_20.jpg)
![Uniform20](noise/hist_uniform_-20_20.jpg)

![Uniform40](noise/boats_uniform_-40_40.jpg)
![Uniform40](noise/hist_uniform_-40_40.jpg)

![Uniform60](noise/boats_uniform_-60_60.jpg)
![Uniform60](noise/hist_uniform_-60_60.jpg)



Salt and pepper šum (redom vrijednosti [5%, 10%, 15%, 20%])

![SnP5](noise/boats_salt_and_pepper_5.jpg)
![SnP5](noise/hist_salt_and_pepper_5.jpg)

![SnP10](noise/boats_salt_and_pepper_10.jpg)
![SnP10](noise/hist_salt_and_pepper_10.jpg)

![SnP15](noise/boats_salt_and_pepper_15.jpg)
![SnP15](noise/hist_salt_and_pepper_15.jpg)

![SnP20](noise/boats_salt_and_pepper_20.jpg)
![SnP20](noise/hist_salt_and_pepper_20.jpg)


Iz histograma je moguće vidjeti utjecaj promjene parametara svakog od šumova.



## Median filter

***Dodavanje šuma na slike te prolazak različitim median i Gaussovim filterima po njima***

```python
import numpy as np
import cv2

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)
  
boats = cv2.imread('../../slike/boats.bmp', 0)
airplane = cv2.imread('../../slike/airplane.bmp', 0)

images = [boats, airplane]
names = ['boats', 'airplane']
sigmas = [5, 15, 35]
salt_n_pepper = [1, 10]
neighborhoods = [3, 5, 7, 11, 15]
gaussian_kernel_sizes = [3, 5, 7]
gaussian_blur_sigmas = [5, 15, 35]

for i in range(len(images)):
    for radius in neighborhoods:
        for sigma in sigmas:
            cv2.imwrite(names[i] + '_gaussian_sigma_' + str(sigma) + '_median_' + str(radius) + '.jpg', cv2.medianBlur(gaussian_noise(images[i], 0, sigma), radius))
            
        for percent in salt_n_pepper:
            cv2.imwrite(names[i] + '_salt_and_pepper_' + str(percent) + '_median_' + str(radius) + '.jpg', cv2.medianBlur(salt_n_pepper_noise(images[i], percent), radius))
            
    for kernel in gaussian_kernel_sizes:
        for blurSigma in gaussian_blur_sigmas:
            for sigma in sigmas:
                cv2.imwrite(names[i] + '_gaussian_sigma_' + str(sigma) + '_gaussian_' + str(kernel) + '_' + str(blurSigma) + '.jpg', cv2.GaussianBlur(gaussian_noise(images[i], 0, sigma), (kernel, kernel), blurSigma))
            
            for percent in salt_n_pepper:
                cv2.imwrite(names[i] + '_salt_and_pepper_' + str(percent) + '_gaussian_' + str(kernel) + '_' + str(blurSigma) + '.jpg', cv2.GaussianBlur(salt_n_pepper_noise(images[i], percent), (kernel, kernel), blurSigma))
```



***Rezultati (zbog velikog broja slika prikazani su određeni rezultati na slici `boats`)***

Gaussov šum (sigma = 15) i median filtar (radius = 3)

![G15M3](medianFiltering/boats_gaussian_sigma_15_median_3.jpg)

Gaussov šum (sigma = 15) i median filtar (radius = 5)

![G15M5](medianFiltering/boats_gaussian_sigma_15_median_5.jpg)

Gaussov šum (sigma = 35) i median filtar (radius = 3)

![G35M3](medianFiltering/boats_gaussian_sigma_35_median_3.jpg)

Gaussov šum (sigma = 35) i median filtar (radius = 5)

![G35M5](medianFiltering/boats_gaussian_sigma_35_median_5.jpg)

Salt and pepper šum i median filtar (radius = 3)

![SNP10M3](medianFiltering/boats_salt_and_pepper_10_median_3.jpg)

Salt and pepper šum i median filtar (radius = 5)

![SNP10M5](medianFiltering/boats_salt_and_pepper_10_median_5.jpg)

Salt and pepper šum i median filtar (radius = 7)

![SNP10M7](medianFiltering/boats_salt_and_pepper_10_median_7.jpg)

Salt and pepper šum i median filtar (radius = 15)

![SNPM15](medianFiltering/boats_salt_and_pepper_10_median_15.jpg)

Gaussov šum (sigma = 35) i Gaussov filtar (3x3, sigma = 5)

![G35F35](medianFiltering/boats_gaussian_sigma_35_gaussian_3_5.jpg)

Gaussov šum (sigma = 35) i Gaussov filtar (3x3, sigma = 15)

![G35F315](medianFiltering/boats_gaussian_sigma_35_gaussian_3_15.jpg)

Gaussov šum (sigma = 35) i Gaussov filtar (5x5, sigma = 5)

![G35F55](medianFiltering/boats_gaussian_sigma_35_gaussian_5_5.jpg)

Gaussov šum (sigma = 35) i Gaussov filtar (5x5, sigma = 15)

![G35F515](medianFiltering/boats_gaussian_sigma_35_gaussian_5_15.jpg)

Gaussov šum (sigma = 35) i Gaussov filtar (7x7, sigma = 5)

![G35F75](medianFiltering/boats_gaussian_sigma_35_gaussian_7_5.jpg)

Gaussov šum (sigma = 35) i Gaussov filtar (7x7, sigma = 15)

![G35F715](medianFiltering/boats_gaussian_sigma_35_gaussian_7_15.jpg)

Gaussov šum (sigma = 35) i Gaussov filtar (5x5, sigma = 5)

![G35F55](medianFiltering/boats_gaussian_sigma_35_gaussian_5_5.jpg)

Salt and pepper šum i Gaussov filtar (3x3, sigma = 15)

![SNP10G315](medianFiltering/boats_salt_and_pepper_10_gaussian_3_15.jpg)


Ostatak slika nalazi se u folderu `medianFiltering`. Vidljivo je da povećanjem radijusa kod median filtra slika postaje sve više zamućena. Median filtar je idealan za uklanjanje salt and pepper šuma, a Gaussov filtar za uklanjanje Gaussovog šuma.



***Vlastiti median filter***

```python
import numpy as np
import cv2

#funkcija za dodavanje dodatnog obruba na sliku kako prilikom korištenja filtera ne bi došlo do problema s granicama
def addBorders(arr, radius):
    newArr = np.zeros((arr.shape[0]+2*radius, arr.shape[1]+2*radius))
    newArr[radius:arr.shape[0]+radius, radius:arr.shape[1]+radius] = arr
    newArr[radius:arr.shape[0]+radius, 0:radius] = arr[:, 0:1]
    newArr[radius:arr.shape[0]+radius, arr.shape[1]+radius:arr.shape[1]+2*radius] = arr[:, arr.shape[1]-1:arr.shape[1]]
    newArr[0:radius, radius:arr.shape[1]+radius] = arr[0:1, :]
    newArr[arr.shape[0]+radius:arr.shape[0]+2*radius, radius:arr.shape[1]+radius] = arr[arr.shape[0]-1:arr.shape[0], :]
    newArr[0:radius, 0:radius] = arr[0, 0]
    newArr[0:radius, arr.shape[1]+radius:arr.shape[1]+2*radius] = arr[0, -1]
    newArr[arr.shape[0]+radius:arr.shape[0]+2*radius, 0:radius] = arr[-1, 0]
    newArr[arr.shape[0]+radius:arr.shape[0]+2*radius, arr.shape[1]+radius:arr.shape[1]+2*radius] = arr[-1, -1]
    return newArr

def myBlur(arr, radius):
    newArr = addBorders(arr, int(radius-1/2))
    for i in range(arr.shape[0]):
        for j in range(arr.shape[1]):
            arr[i,j] = np.median(newArr[i:i+radius, j:j+radius])
    return arr

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

img = cv2.imread("../../slike/boats.bmp", 0)
img = salt_n_pepper_noise(img, 20)
cv2.imshow("Original",img)
cv2.imshow("OpenCV",cv2.medianBlur(img,3))
cv2.imshow("Custom verzija",myBlur(img,3)) #daje isti rezultat
```



Vlastita implementacija daje isti rezultat kao i OpenCV, ali je znatno sporija.



## Thresholding

***Kod***

```python
import cv2

boats = cv2.imread('../../slike/boats.bmp', 0)
baboon = cv2.imread('../../slike/baboon.bmp', 0)
airplane = cv2.imread('../../slike/airplane.bmp', 0)
images = [boats, baboon, airplane]
imgTitles = ['boats', 'baboon', 'airplane']
thresholds = [80, 127, 170]

for threshold in thresholds:
    for i in range(len(images)):
        ret,thresh1 = cv2.threshold(images[i],threshold,255,cv2.THRESH_BINARY)
        ret,thresh2 = cv2.threshold(images[i],threshold,255,cv2.THRESH_BINARY_INV)
        ret,thresh3 = cv2.threshold(images[i],threshold,255,cv2.THRESH_TRUNC)
        ret,thresh4 = cv2.threshold(images[i],threshold,255,cv2.THRESH_TOZERO)
        ret,thresh5 = cv2.threshold(images[i],threshold,255,cv2.THRESH_TOZERO_INV)
        thresh6 = cv2.adaptiveThreshold(images[i],255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,11,2)
        thresh7 = cv2.adaptiveThreshold(images[i],255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
      
        titles = ['BINARY','BINARY_INV','TRUNC','TOZERO','TOZERO_INV']
        newImages = [thresh1, thresh2, thresh3, thresh4, thresh5]
        
        for j in range(len(newImages)):
            cv2.imwrite(imgTitles[i] + '_' + titles[j] + '_thr_' + str(threshold) + '.jpg', newImages[j])

for i in range(len(images)):
    thresh6 = cv2.adaptiveThreshold(images[i],255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,11,2)
    thresh7 = cv2.adaptiveThreshold(images[i],255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
    ret,thresh8 = cv2.threshold(images[i],0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    print(ret)
    
    titles = ['ADAPTIVE_MEAN', 'ADAPTIVE_GAUSSIAN', 'OTSU']
    newImages = [thresh6, thresh7, thresh8]
    
    for j in range(len(newImages)):
        cv2.imwrite(imgTitles[i] + '_' + titles[j] + '.jpg', newImages[j])
```



***Rezultati (za sliku `baboon`)***

BINARY (threshold = 80)

![B80](thresholding/baboon_BINARY_thr_80.jpg)

BINARY (threshold = 127)

![B127](thresholding/baboon_BINARY_thr_127.jpg)

BINARY (threshold = 170)

![B170](thresholding/baboon_BINARY_thr_170.jpg)

BINARY_INV (threshold = 80)

![BI80](thresholding/baboon_BINARY_INV_thr_80.jpg)

BINARY_INV (threshold = 127)

![BI127](thresholding/baboon_BINARY_INV_thr_127.jpg)

BINARY_INV (threshold = 170)

![BI170](thresholding/baboon_BINARY_INV_thr_170.jpg)

TRUNC (threshold = 80)

![T80](thresholding/baboon_TRUNC_thr_80.jpg)

TRUNC (threshold = 127)

![T127](thresholding/baboon_TRUNC_thr_127.jpg)

TRUNC (threshold = 170)

![T170](thresholding/baboon_TRUNC_thr_170.jpg)

TOZERO (threshold = 80)

![TZ80](thresholding/baboon_TOZERO_thr_80.jpg)

TOZERO (threshold = 127)

![TZ127](thresholding/baboon_TOZERO_thr_127.jpg)

TOZERO (threshold = 170)

![TZ170](thresholding/baboon_TOZERO_thr_170.jpg)

TOZERO_INV (threshold = 80)

![TZI80](thresholding/baboon_TOZERO_INV_thr_80.jpg)

TOZERO_INV (threshold = 127)

![TZI127](thresholding/baboon_TOZERO_INV_thr_127.jpg)

TOZERO_INV (threshold = 170)

![TZI170](thresholding/baboon_TOZERO_INV_thr_170.jpg)

ADAPTIVE_GAUSSIAN 

![AG](thresholding/baboon_ADAPTIVE_GAUSSIAN.jpg)

ADAPTIVE_MEAN 

![AM](thresholding/baboon_ADAPTIVE_MEAN.jpg)

OTSU

![O](thresholding/baboon_OTSU.jpg)



Može se primjetiti kako Otsuova binarizacija daje bolju sliku nego ručno odabrane vrijednosti praga za binarni thresholding što je njezina glavna prednost. Postupci adaptivnog thresholdinga daju podjednake rezultate, ali mean thresholding daje grublju sliku, odnosno više ističe rubove na njoj.

Ostatak rezultata nalazi se u mapi `thresholding`.