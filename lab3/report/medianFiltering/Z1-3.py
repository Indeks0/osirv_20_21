import numpy as np
import cv2

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)
  
boats = cv2.imread('../../slike/boats.bmp', 0)
airplane = cv2.imread('../../slike/airplane.bmp', 0)

images = [boats, airplane]
names = ['boats', 'airplane']
sigmas = [5, 15, 35]
salt_n_pepper = [1, 10]
neighborhoods = [3, 5, 7, 11, 15]
gaussian_kernel_sizes = [3, 5, 7]
gaussian_blur_sigmas = [5, 15, 35]

for i in range(len(images)):
    for radius in neighborhoods:
        for sigma in sigmas:
            cv2.imwrite(names[i] + '_gaussian_sigma_' + str(sigma) + '_median_' + str(radius) + '.jpg', cv2.medianBlur(gaussian_noise(images[i], 0, sigma), radius))
            
        for percent in salt_n_pepper:
            cv2.imwrite(names[i] + '_salt_and_pepper_' + str(percent) + '_median_' + str(radius) + '.jpg', cv2.medianBlur(salt_n_pepper_noise(images[i], percent), radius))
            
    for kernel in gaussian_kernel_sizes:
        for blurSigma in gaussian_blur_sigmas:
            for sigma in sigmas:
                cv2.imwrite(names[i] + '_gaussian_sigma_' + str(sigma) + '_gaussian_' + str(kernel) + '_' + str(blurSigma) + '.jpg', cv2.GaussianBlur(gaussian_noise(images[i], 0, sigma), (kernel, kernel), blurSigma))
            
            for percent in salt_n_pepper:
                cv2.imwrite(names[i] + '_salt_and_pepper_' + str(percent) + '_gaussian_' + str(kernel) + '_' + str(blurSigma) + '.jpg', cv2.GaussianBlur(salt_n_pepper_noise(images[i], percent), (kernel, kernel), blurSigma))