## Problem 1

##### Spajanje slika 

```python
import numpy as np
import cv2

img1 = cv2.imread('../../slike/airplane.bmp')
img2 = cv2.imread('../../slike/baboon.bmp')
img3 = cv2.imread('../../slike/pepper.bmp')

minHeight = np.min([img1.shape[0], img2.shape[0], img3.shape[0]])

img1 = img1[:minHeight,:,:]
img2 = img2[:minHeight,:,:]
img3 = img3[:minHeight,:,:]

rez = np.hstack((img1, img2, img3))

cv2.imwrite('spojene_slike.bmp', rez)
cv2.imshow('Spojene slike', rez)
cv2.waitKey(0)
cv2.destroyAllWindows()
```

***Rezultat***

![Problem 1 pt. 1](problem1/spojene_slike.bmp)



##### Dodavanje obruba

```python
def addFrame(img, frameWidth):
    height = img.shape[0]
    width = img.shape[1]
    img = np.hstack((np.zeros((height, frameWidth, 3), dtype = np.uint8), img, np.zeros((height, frameWidth, 3), dtype = np.uint8)))
    img = np.vstack((np.zeros((frameWidth, width + 2 * frameWidth, 3), dtype = np.uint8), img, np.zeros((frameWidth, width + 2 * frameWidth, 3), dtype = np.uint8)))
    return img
    
framedImg = cv2.imread('../../slike/pepper.bmp')
framedImg = addFrame(framedImg, 20)
cv2.imwrite('pepper_frame_20px.bmp', framedImg)
```

***Rezultat***

![Problem 1 pt. 2](problem1/pepper_frame_20px.bmp)



## Problem 2

```python
import numpy as np
import cv2

def convolve(image, kernel):
    output = np.zeros( (image.shape[0] -kernel.shape[0] + 1,
                        image.shape[1] - kernel.shape[1] +1))
    kernel_rev = kernel[::-1,::-1]


    for i in range(0, output.shape[0]):
        for j in range(0, output.shape[1]):
            for k in range(kernel.shape[0]):
                for l in range(kernel.shape[1]):
                    output[i,j] += image[i+k,j+l] * kernel_rev[k,l]

    output[output>255] = 255
    output[output<0]   = 0
    output = output.astype(np.uint8)
    return output 

img = cv2.imread('../../slike/pepper.bmp', 0)
cv2.imwrite('pepper_identity.bmp', convolve(img, np.array([[0,0,0],[0,1,0],[0,0,0]])))
cv2.imwrite('pepper_edgeDetection1.bmp', convolve(img, np.array([[1,0,-1],[0,0,0],[-1,0,1]])))
cv2.imwrite('pepper_edgeDetection2.bmp', convolve(img, np.array([[0,-1,0],[-1,4,-1],[0,-1,0]])))
cv2.imwrite('pepper_edgeDetection3.bmp', convolve(img, np.array([[-1,-1,-1],[-1,8,-1],[-1,-1,-1]])))
cv2.imwrite('pepper_sharpen.bmp', convolve(img, np.array([[0,-1,0],[-1,5,-1],[0,-1,0]])))
cv2.imwrite('pepper_boxBlur.bmp', convolve(img, 1/9*np.array([[1,1,1],[1,1,1],[1,1,1]])))
cv2.imwrite('pepper_gaussian3x3.bmp', convolve(img, 1/16*np.array([[1,2,1],[2,4,2],[1,2,1]])))
cv2.imwrite('pepper_gaussian5x5.bmp', convolve(img, 1/256*np.array([[1,4,6,4,1],[4,16,24,16,4],[6,24,36,24,6],[4,16,24,16,4],[1,4,6,4,1]])))
cv2.imwrite('pepper_unsharp.bmp', convolve(img, -1/256*np.array([[1,4,6,4,1],[4,16,24,16,4],[6,24,-476,24,6],[4,16,24,16,4],[1,4,6,4,1]])))
```

***Rezultati***

Identity

![Identity](problem2/pepper_identity.bmp)



Edge detection v1

![Edge1](problem2/pepper_edgeDetection1.bmp)



Edge detection v2

![Edge2](problem2/pepper_edgeDetection2.bmp)



Edge detection v3

![Edge3](problem2/pepper_edgeDetection3.bmp)



Sharpen

![Sharpen](problem2/pepper_sharpen.bmp)



Box blur

![Box](problem2/pepper_boxBlur.bmp)



Gaussian blur 3x3

![Gauss3](problem2/pepper_gaussian3x3.bmp)



Gaussian blur 5x5

![Gauss5](problem2/pepper_gaussian5x5.bmp)



Unsharp masking 5x5

![Unsharp](problem2/pepper_unsharp.bmp)



## Problem 3

```python
import cv2
import os

imgs = os.listdir('../../slike')

for img in imgs:
    cv2.imwrite(img.split('.')[0] + '_invert.png', 255 - cv2.imread('../../slike/'+ img, 0))
```

***Rezultati (za 3 slike)***

![Invert A](problem3/airplane_invert.png)



![Invert bbn](problem3/baboon_invert.png)



![Invert B](problem3/barbara_invert.png)



## Problem 4

```python
import cv2
import os

imgs = os.listdir('../../slike')
thresholds = [63, 127, 191]

for imgName in imgs:
    img = cv2.imread('../../slike/' + imgName, 0)
    for threshold in thresholds:
        thr_img = img.copy()
        thr_img[thr_img < threshold] = 0
        cv2.imwrite(imgName.split('.')[0] + '_' + str(threshold) + '_thresh.png', thr_img)
```

***Rezultati (slika 'pepper')***

Prag 63

![Threshold](problem4/pepper_63_thresh.png)



Prag 127

![Threshold2](problem4/pepper_127_thresh.png)



Prag 191

![Threshold3](problem4/pepper_191_thresh.png)



## Problem 5

```python
import cv2
import numpy as np

img = cv2.imread('../../slike/BoatsColor.bmp', 0)

for q in range(1,9):
    d = 2 ** (8-q)
    new_img = img.copy()
    new_img = new_img.astype(np.float32)
    new_img = (np.floor(new_img / d) + 0.5) * d
    new_img[new_img > 255] = 255
    new_img[new_img < 0] = 0
    new_img = new_img.astype(np.uint8)
    cv2.imwrite('boats_' + str(q) + '.bmp', new_img)

# Opadanje kvalitete slike postaje vidljivo tek pri vrijednosti q=4 (vidljivo na nebu),
# a prvo primjetljivije opadanje kvalitete dijela slike na kojem su brodovi vidljivo je za q=2.
# Dok je pri vrijednosti q=2 još lako razaznati što se nalazi na slici, za q=1 i to postaje problem,
# no i dalje se može doći do zaključka da se na slici nalaze brodovi.
```

***Rezultati***

q = 8

![q8](problem5/boats_8.bmp)



q = 7

![q7](problem5/boats_7.bmp)



q = 6

![q6](problem5/boats_6.bmp)



q = 5

![q5](problem5/boats_5.bmp)



q = 4

![q4](problem5/boats_4.bmp)



q = 3

![q3](problem5/boats_3.bmp)



q = 2

![q2](problem5/boats_2.bmp)



q = 1

![q1](problem5/boats_1.bmp)



## Problem 6

```python
import numpy as np
import cv2

img = cv2.imread('../../slike/BoatsColor.bmp', 0)

for q in range(1,9):
    d = 2 ** (8-q)
    noise = np.random.uniform(-0.5, 0.5, img.shape)
    new_img = img.copy()
    new_img = new_img.astype(np.float32)
    new_img = (np.floor(new_img / d + noise) + 0.5) * d
    #u sljedeće 2 linije se vrijednosti izvan intervala [0,255] postavljaju na 255, odnosno 0 te odmah kvantiziraju na odgovarajuću vrijednost
    new_img[new_img > 255] = (np.floor(255 / d) + 0.5) * d
    new_img[new_img < 0] = (np.floor(0 / d) + 0.5) * d
    new_img = new_img.astype(np.uint8)
    cv2.imwrite('boats_' + str(q) + 'n.bmp', new_img)
    
# Kao i u prethodnom zadatku, opadanje kvalitete slike nije vidljivo za vrijednosti q
# veće od 4. Za vrijednosti q=4 i q=3 počinje se primjećivati šum na slici, no sama
# kvaliteta slike je i dalje visoka (dio slike s nebom izgleda bolje nego za iste
# vrijednosti q u prošlom zadatku). Utjecaj šuma naglo se povećava za q=2, no slika
# je i dalje donekle razlučiva, dok za q=1 utjecaj šuma postaje vrlo visok te se gube
# mnogi detalji na slici, ali je i dalje moguće razaznati objekte na slici, iako s
# većim poteškoćama nego prije. Općenito, za niske vrijednosti q slika sa šumom
# ostala je sličnija početnoj slici nego slika bez šuma jer utjecaj šuma daje dojam
# većeg broja sivih vrijednosti (miješanjem svijetlih i tamnih piksela).
```

***Rezultati***

q = 8

![qn8](problem6/boats_8n.bmp)



q = 7

![qn7](problem6/boats_7n.bmp)



q = 6

![qn6](problem6/boats_6n.bmp)



q = 5

![qn5](problem6/boats_5n.bmp)



q = 4

![qn4](problem6/boats_4n.bmp)



q = 3

![qn3](problem6/boats_3n.bmp)



q = 2

![qn2](problem6/boats_2n.bmp)



q = 1

![qn1](problem6/boats_1n.bmp)



## Problem 7

```python
import cv2
import numpy as np

img = cv2.imread('../../slike/baboon.bmp', 0)
img = cv2.resize(img, None, fx=0.25, fy=0.25, interpolation = cv2.INTER_CUBIC)
rows, cols = img.shape
imgs = []

for angle in range(0, 360, 30):
    M = cv2.getRotationMatrix2D((cols/2, rows/2), angle, 1)
    dst = cv2.warpAffine(img, M, (cols, rows))
    imgs.append(dst)

rez = np.hstack(imgs)
cv2.imwrite('baboon_rotated.bmp', rez)
```

***Rezultat***

![rotate](problem7/baboon_rotated.bmp)