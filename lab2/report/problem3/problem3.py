import cv2
import os

imgs = os.listdir('../../slike')

for img in imgs:
    cv2.imwrite(img.split('.')[0] + '_invert.png', 255 - cv2.imread('../../slike/'+ img, 0))