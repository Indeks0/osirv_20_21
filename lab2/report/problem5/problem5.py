import cv2
import numpy as np

img = cv2.imread('../../slike/BoatsColor.bmp', 0)

for q in range(1,9):
    d = 2 ** (8-q)
    new_img = img.copy()
    new_img = new_img.astype(np.float32)
    new_img = (np.floor(new_img / d) + 0.5) * d
    new_img[new_img > 255] = 255
    new_img[new_img < 0] = 0
    new_img = new_img.astype(np.uint8)
    cv2.imwrite('boats_' + str(q) + '.bmp', new_img)

# Opadanje kvalitete slike postaje vidljivo tek pri vrijednosti q=4 (vidljivo na nebu),
# a prvo primjetljivije opadanje kvalitete dijela slike na kojem su brodovi vidljivo je za q=2.
# Dok je pri vrijednosti q=2 još lako razaznati što se nalazi na slici, za q=1 i to postaje problem,
# no i dalje se može doći do zaključka da se na slici nalaze brodovi.