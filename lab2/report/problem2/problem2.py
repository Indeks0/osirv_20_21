import numpy as np
import cv2

def convolve(image, kernel):
    output = np.zeros( (image.shape[0] -kernel.shape[0] + 1,
                        image.shape[1] - kernel.shape[1] +1))
    kernel_rev = kernel[::-1,::-1]


    for i in range(0, output.shape[0]):
        for j in range(0, output.shape[1]):
            for k in range(kernel.shape[0]):
                for l in range(kernel.shape[1]):
                    output[i,j] += image[i+k,j+l] * kernel_rev[k,l]

    output[output>255] = 255
    output[output<0]   = 0
    output = output.astype(np.uint8)
    return output 

img = cv2.imread('../../slike/pepper.bmp', 0)
cv2.imwrite('pepper_identity.bmp', convolve(img, np.array([[0,0,0],[0,1,0],[0,0,0]])))
cv2.imwrite('pepper_edgeDetection1.bmp', convolve(img, np.array([[1,0,-1],[0,0,0],[-1,0,1]])))
cv2.imwrite('pepper_edgeDetection2.bmp', convolve(img, np.array([[0,-1,0],[-1,4,-1],[0,-1,0]])))
cv2.imwrite('pepper_edgeDetection3.bmp', convolve(img, np.array([[-1,-1,-1],[-1,8,-1],[-1,-1,-1]])))
cv2.imwrite('pepper_sharpen.bmp', convolve(img, np.array([[0,-1,0],[-1,5,-1],[0,-1,0]])))
cv2.imwrite('pepper_boxBlur.bmp', convolve(img, 1/9*np.array([[1,1,1],[1,1,1],[1,1,1]])))
cv2.imwrite('pepper_gaussian3x3.bmp', convolve(img, 1/16*np.array([[1,2,1],[2,4,2],[1,2,1]])))
cv2.imwrite('pepper_gaussian5x5.bmp', convolve(img, 1/256*np.array([[1,4,6,4,1],[4,16,24,16,4],[6,24,36,24,6],[4,16,24,16,4],[1,4,6,4,1]])))
cv2.imwrite('pepper_unsharp.bmp', convolve(img, -1/256*np.array([[1,4,6,4,1],[4,16,24,16,4],[6,24,-476,24,6],[4,16,24,16,4],[1,4,6,4,1]])))