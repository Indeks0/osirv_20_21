import numpy as np
import cv2

img = cv2.imread('../../slike/BoatsColor.bmp', 0)

for q in range(1,9):
    d = 2 ** (8-q)
    noise = np.random.uniform(-0.5, 0.5, img.shape)
    new_img = img.copy()
    new_img = new_img.astype(np.float32)
    new_img = (np.floor(new_img / d + noise) + 0.5) * d
    #u sljedeće 2 linije se vrijednosti izvan intervala [0,255] postavljaju na 255, odnosno 0 te odmah kvantiziraju na odgovarajuću vrijednost
    new_img[new_img > 255] = (np.floor(255 / d) + 0.5) * d
    new_img[new_img < 0] = (np.floor(0 / d) + 0.5) * d
    new_img = new_img.astype(np.uint8)
    cv2.imwrite('boats_' + str(q) + 'n.bmp', new_img)
    
# Kao i u prethodnom zadatku, opadanje kvalitete slike nije vidljivo za vrijednosti q
# veće od 4. Za vrijednosti q=4 i q=3 počinje se primjećivati šum na slici, no sama
# kvaliteta slike je i dalje visoka (dio slike s nebom izgleda bolje nego za iste
# vrijednosti q u prošlom zadatku). Utjecaj šuma naglo se povećava za q=2, no slika
# je i dalje donekle razlučiva, dok za q=1 utjecaj šuma postaje vrlo visok te se gube
# mnogi detalji na slici, ali je i dalje moguće razaznati objekte na slici, iako s
# većim poteškoćama nego prije. Općenito, za niske vrijednosti q slika sa šumom
# ostala je sličnija početnoj slici nego slika bez šuma jer utjecaj šuma daje dojam
# većeg broja sivih vrijednosti (miješanjem svijetlih i tamnih piksela).